<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'id' => 1,
            'post_id' => 1,
            //'parent_id' => 1,
            'creator_id' => 1,
            'creator_type' => 'test',
            'commentable_id' => 1,
            'commentable_type' => 'test',
            'body' => str_random(50),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
