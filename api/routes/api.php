<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'AccountController@register');
Route::post('/login', 'AccountController@login');

Route::prefix('posts')->group(function () {
    Route::get('/', 'PostController@index')->name('posts.all'); //DONE
    Route::post('/', 'PostController@store')->name('posts.create'); //DONE
    Route::get('/{post}', 'PostController@show')->name('posts.get'); //DONE
    Route::patch('/{post}', 'PostController@update')->name('posts.update'); //DONE @TODO check if auth owns
    Route::delete('/{post}', 'PostController@destroy')->name('posts.delete'); //DONE @todo check if auth owns

    Route::get('/{post}/comments', 'CommentController@index')->name('comments.all'); //DONE
    Route::post('/{post}/comments', 'CommentController@store')->name('comments.create'); //DONE
    Route::patch('/{post}/comments/{comment}', 'CommentController@update')->name('comments.update'); //DONE @TODO convert owner checking to middleware
    Route::delete('/{post}/comments/{comment}', 'CommentController@destroy')->name('comments.delete'); //DONE  @TODO convert owner checking to middleware
});
