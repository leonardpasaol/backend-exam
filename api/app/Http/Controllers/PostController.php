<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePost;
use Illuminate\Http\Request;
use App\Http\Resources\PostCollection;
use Illuminate\Support\Facades\Auth;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new PostCollection(Post::paginate());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $validated = $request->validated();

        $user = Auth::user();

        $post = new Post;
        $post->title = $validated['title'];
        $post->slug = str_replace(' ', '-', $request->title);
        $post->image = $request->image;
        $post->content = $request->content;

        $user->posts()->save($post);

        return response()->json([
            'data' => $post
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', '=', $slug)->firstOrFail();
        return response()->json([
            'data' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePost $request, $slug)
    {
        //TODO check if post->user_id == auth->user->id
        $validated = $request->validated();

        $user = Auth::user();
        $post = Post::where('slug', '=', $slug)->first();

        $post->title = $validated['title'];
        $post->slug = str_replace(' ', '-', $request->title);
        $post->image = $request->image;
        $post->content = $request->content;

        $post->save();
        $post->touch();

        return response()->json([
            'data' => $post
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //TODO check if post->user_id == auth->user->id
        $post->delete();
        return response()->json([
            'status' => "record deleted successfully"
        ]);
    }
}
