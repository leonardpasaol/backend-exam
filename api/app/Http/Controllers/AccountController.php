<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserAccount;
use App\Http\Requests\UserLogin;
use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public $successStatus = 200;
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(StoreUserAccount $request)
    {
        $validated = $request->validated();

        $user = new User;
        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->password = bcrypt($validated['password']);

        $user->save();

        return response()->json($user, 201);
    }

    public function login(UserLogin $request)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $tokenObj =  $user->createToken('MyApp');
            $token = $tokenObj->accessToken;
            $expiration = $tokenObj->token->expires_at;

            return response()->json([
                'token' => $token,
                'token_type' => 'bearer',
                'expires_at' => $expiration,
            ], $this-> successStatus);
        } else {
            return response()->json(['error'=>'Unauthorised'], 422);
        }
    }
}
