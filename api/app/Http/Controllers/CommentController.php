<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Comment as CommentCollection;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\Post;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($postTitle)
    {
        $post = Post::where('slug', '=', $postTitle)->first();
        return new CommentCollection($post->comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $user = Auth::user();
        $comment = new Comment;
        $comment->body = $request->body;
        $comment->creator_id = $user->id;
        $comment->creator_type = $user;
        $comment->commentable_id = $post->id;
        $comment->commentable_type = $post;
        $comment->parent_id = $request->parent_id;

        $post->comments()->save($comment);

        return response()->json([
            'data' => $comment
        ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Comment  $comment
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $postSlug, Comment $comment)
    {
        $user = Auth::user();
        $post = Post::where('slug', '=', $postSlug)->firstOrFail();

        if ($comment->creator_id === $user->id && $post->user_id === $comment->creator_id) {
            $comment->body = $request->body;
            $comment->save();
            $comment->touch();

            return response()->json([
                'data' => $comment
            ]);
        } else {
            return response()->json([
                'message' => 'Unauthorized'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $postSlug, Comment $comment)
    {
        $user = Auth::user();
        $post = Post::where('slug', '=', $postSlug)->firstOrFail();

        if ($comment->creator_id === $user->id && $post->user_id === $comment->creator_id) {
            $comment->delete();

            return response()->json([
                'status' => "Record deleted succesfully."
            ]);
        } else {
            return response()->json([
                'message' => 'Unauthorized'
            ]);
        }
    }
}
